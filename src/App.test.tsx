import { render, screen } from '@testing-library/react';
import Title from './components/Title';

test('renders learn react link', () => {
  render(<Title>app</Title>);
  const linkElement = screen.getByText(/app/i);
  expect(linkElement).toBeInTheDocument();
});
