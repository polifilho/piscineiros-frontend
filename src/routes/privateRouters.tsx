import { Navigate } from "react-router-dom";

export default function privateRoutes({ Component, isAuth, ...rest }: any) {
  return isAuth ? <Component {...rest} /> : <Navigate to="/login" replace />;
}