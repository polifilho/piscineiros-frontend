export interface AddressDetails {
  cep: string;
  street: string;
  city: string;
  complement: string;
  number: string;
  state: string;
  district: string;
}

export interface Customer {
    _id?: string;
    name: string;
    cpf: string | null;
    cnpj: string | null;
    email: string;
    phone: string;
    address: AddressDetails,
    gender: string;
    datePaymentExpired: string;
    paymentMethod: string;
    price: string;
    services: string[];
    customerType: string;
    paymentOften: string;
    paymentStatus: string;
    serviceDays: string[];
    periodOfTime: string[];
    realDatePaymentExpired: number;
    dateToCreateCustomer?: number;
    timeCustomerPriceSave?: string;
    lastUpdate?: number;
    providerId?: string;
}

export interface IAlerts {
  status: "error" | "success" | "info" | "warning" | undefined;
  message: string;
  open: boolean
}

export interface Provider {
  name: string;
  cpf: string;
  email: string;
  phone: string;
  cnpj: string;
  companyName: string;
  password: string;
  repeatPassword: string;
  services: string[];
  gender: string;
  profiles?: string[];
  avatar?: any;
}
